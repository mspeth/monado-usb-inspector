# Monado USB Inspector

A simple python tool for reading the USB bus to help develop drivers for Monado

## Requirements

* Python 3.10+
* Pipenv

## Install

```bash
pipenv install
```

## Usage

TODO
