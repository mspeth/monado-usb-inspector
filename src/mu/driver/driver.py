import usb.core
import usb.backend.libusb1
import usb.util
from mu.usb_util import get_device
import sys
from hexdump import hexdump

class Driver:

    def __init__(self, vendor_id, product_id, product_name, endpoint_address=None, hid_interface=0):
        self.vendor_id = vendor_id
        self.product_id = product_id
        self.product_name = product_name
        self.endpoint_address = endpoint_address

        self.device = get_device(vendor_id,product_id,product_name)
        if self.device is None:
            sys.exit(1)
        print(self.device)

        # Claim interface 0 - this interface provides IN and OUT endpoints to write to and read from
        #usb.util.claim_interface(device, 0)

        #device.set_configuration()

        #cfg = device.get_active_configuration()
        for cfg in self.device:
            for intf in cfg:
                #print("\t", str(intf.bInterfaceNumber) , ','+str(intf.bAlternateSetting))
                for ep in intf:
                    if self.endpoint_address == None:
                        self.endpoint_address = ep.bEndpointAddress
                    #print("\t\t",str(hex(ep.bEndpointAddress)))

        self.device.reset()

        if self.device.is_kernel_driver_active(hid_interface):
            try:
                self.device.detach_kernel_driver(hid_interface)
            except usb.core.USBError as e:
                sys.exit("Could not detatch kernel driver")

        #usb.util.release_interface(self.device,0)
        #cfg = usb.util.find_descriptor(self.device,bConfigurationValue=hid_interface)
        #cfg.set()


    def read(self,bytes,timeout,address=None):
        if address == None:
            address = self.endpoint_address
        try:
            data = self.device.read(address, bytes, timeout)
            #print(data)
        except usb.core.USBError as e:
            print ("Error reading response: {}".format(e.args))
            return None
        #byte_str = ''.join(chr(n) for n in data[1:]) # construct a string out of the read values, starting from the 2nd byte
        #byte_str = ' '.join(hex(n) for n in data) 
        print(hexdump(data))
        return data

        if len(result_str) == 0:
            return None

        return result_str

    def write(self,endpoint_address, data):
        self.device.write(endpoint_address,data)

