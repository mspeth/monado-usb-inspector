from mu.driver.driver import Driver

class Pimax(Driver):
    #address = 0x81
    #interface = 0
    address = 0x82
    interface = 1

    def __init__(self):
        super().__init__(0x0483,0x0101,"Pimax P2EA",self.address,self.interface)

    def hid_set_report(self, dev, report,interface):
      """ Implements HID SetReport via USB control transfer """
      dev.ctrl_transfer(
          0x21,  # REQUEST_TYPE_CLASS | RECIPIENT_INTERFACE | ENDPOINT_OUT
          9,     # SET_REPORT
          self.vendor_id, # "Vendor" Descriptor Type + 0 Descriptor Index
          interface,     # USB interface № 0
          report # the HID payload as a byte array -- e.g. from struct.pack()
      )

    def wakeup(self):
        self.device.set_configuration()
        ## need to set the 
        #bmAttributes         :   0xa0 Bus Powered, Remote Wakeup

        """ A packet to wakeup the device """
        data = [0x11, 0x00, 0x00, 0x0b, 0x10, 0x27]
        message = "\x11\x00\x00\x0b\x10\x27"
        #self.hid_set_report(self.device,data,self.interface)
        self.hid_set_report(self.device,message,self.interface)
        #self.write(0x81,message)
        #self.write(0x82,message)
        print(self.device)


    def read(self,bytes,timeout,address=None):
        self.wakeup()
        super().read(bytes,timeout,address)