import usb.core
import usb.backend.libusb1

def get_device(id,idProduct,product):
    device = usb.core.find(idVendor=id, idProduct=idProduct,product=product)

    if device is None:
        raise ValueError('Device not found. Please the device is connected.')
        return None
    return device